//
//  AddContactVC.swift
//  CalculatorDemo
//
//  Created by PiPyL on 10/16/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

//Buoc 1
protocol AddContactVCDelegate: class {
    func didFinishAddContact(contactModel: ContactModel)
}

class AddContactVC: UIViewController {

    @IBOutlet weak var fullNameTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var mailTF: UITextField!
    
    //Buoc 1: Tao closure
    var didClickAddContact: ((_ contact: ContactModel) -> Void)?
    
    //Buoc 2
    weak var delegate: AddContactVCDelegate?
    
//    var didClickAddContact: ((_ contact: ContactModel, _ value: String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let addBarButton = UIBarButtonItem(image: UIImage(named: "ic_plus"), style: .done, target: self, action: #selector(didClickAddBarButton))
//        self.navigationItem.rightBarButtonItem = addBarButton
        self.navigationItem.title = "Add Contact"
    }
    
    @objc func didClickAddBarButton() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateAddressVC") {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didClickAddButton(_ sender: Any) {
        if let fullName = fullNameTF.text, let phone = phoneTF.text, let mail = mailTF.text {
            let avatar = "ic_avatar4"
            let contactModel = ContactModel(name: fullName, phone: phone, avatar: avatar, mail: mail)
            //Buoc 3 gui su kien di
            didClickAddContact?(contactModel)
            
//            self.didClickAddContact?(contactModel, "Hello")
//            delegate?.didFinishAddContact(contactModel: contactModel)
            self.navigationController?.popViewController(animated: true)
        }
    }
}
