//
//  ContactViewController.swift
//  CalculatorDemo
//
//  Created by PiPyL on 9/26/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var contactList = [ContactModel]()
    var contactListRoot = [ContactModel]()

    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)

        //TableView
        tableView.register(ContactTableViewCell.nib(), forCellReuseIdentifier: ContactTableViewCell.cellIdentifier)
                
        let backBarButton = UIBarButtonItem(image: UIImage(named: "ic_back"), style: .done, target: self, action: #selector(didClickBackBarButton))
        self.navigationItem.leftBarButtonItem = backBarButton
        
        let addBarButton = UIBarButtonItem(image: UIImage(named: "ic_plus"), style: .done, target: self, action: #selector(didClickAddBarButton))
        self.navigationItem.rightBarButtonItem = addBarButton
        
        fetchContacts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK: - Helper
    @objc func didClickBackBarButton() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func didClickAddBarButton() {
        let addContactVC = self.storyboard?.instantiateViewController(withIdentifier: "AddContactVC") as! AddContactVC
        //Buoc 4: Buoc dang ky
        //        addContactVC.delegate = self
        addContactVC.didClickAddContact = { contact in
            self.contactList.append(contact)
            self.tableView.reloadData()
        }
        self.navigationController?.pushViewController(addContactVC, animated: true)
    }
    
    private func deleteContactLocal(_ contact: ContactModel) {
        if let index = contactListRoot.firstIndex(of: contact) {
            contactListRoot.remove(at: index)
        }
        if let index = contactList.firstIndex(of: contact) {
            contactList.remove(at: index)
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    //MARK: - Action
    @IBAction func didClickBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - APIs
    private func fetchContacts() {
        APIController.shared.fetchContacts { (result) in
            switch result {
            case .failure(let error):
                print("Error: \(error.localizedDescription)")
                let alert = UIAlertController(title: "Thông báo", message: "Đã có lỗi xảy ra. Vui lòng thử lại!", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(okAction)
                DispatchQueue.main.async {
                    self.present(alert, animated: true, completion: nil)
                }
                break
            case .success(let contactList):
                self.contactList = contactList
                self.contactListRoot = contactList
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                break
            }
        }
    }
    
    private func deleteContact(_ contact: ContactModel) {
        guard let id = contact.id else {
            return
        }
        APIController.shared.deleteContacts(idContact: id) { [weak self] (error) in
            guard let self = self else { return }
            if let error = error {
                print(error.localizedDescription)
            } else {
                self.deleteContactLocal(contact)
            }
        }
    }
}

// thiet lap tableview
//MARK: - TableView
extension ContactViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let contactModel = contactList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as! ContactTableViewCell
        cell.setupCell(contactModel: contactModel, indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let contactModel = contactList[indexPath.row]
        
        if let contactDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactDetailsVC") as? ContactDetailsVC {
            contactDetailsVC.contactModel = contactModel
            self.navigationController?.pushViewController(contactDetailsVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let contact = contactList[indexPath.row]
        
        let popup = UIAlertController(title: "Thông Báo", message: "Bạn có muốn xóa \(contact.name) hay không?", preferredStyle: .alert)
        
        let noAction = UIAlertAction(title: "Không", style: .default) { action in
            
        }
        
        let yesAction = UIAlertAction(title: "Có", style: .destructive) { [weak self] action in
            guard let self = self else { return }
            self.deleteContact(contact)
        }
        
        popup.addAction(noAction)
        popup.addAction(yesAction)

        self.present(popup, animated: true, completion: nil)
    }
}

//Buoc 5 nhan su kien
//MARK: - AddContactVCDelegate
extension ContactViewController: AddContactVCDelegate {
    func didFinishAddContact(contactModel: ContactModel) {
        contactList.append(contactModel)
        //Lam moi du lieu tableview
        tableView.reloadData()
    }
}

//MARK: - SearchBar Delegate
extension ContactViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            contactList = contactListRoot
            tableView.reloadData()
            return
        }
        let result =  contactListRoot.filter { (contact) -> Bool in
            return contact.name.lowercased().contains(searchText.lowercased())
        }
        contactList = result
        tableView.reloadData()
    }
}


