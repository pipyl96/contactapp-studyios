//
//  UpdateAddressVC.swift
//  CalculatorDemo
//
//  Created by PiPyL on 10/27/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class UpdateAddressVC: UIViewController {

    @IBOutlet weak var addressTF: UITextField!
        
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didClickUpdateAddress(_ sender: Any) {
        //B1: gui di thong bao
        guard let address = addressTF.text else {
            return
        }
        NotificationCenter.default.post(name: NSNotification.Name("DidClickUpdateAddress"), object: nil, userInfo: ["address": address])
    }
}
