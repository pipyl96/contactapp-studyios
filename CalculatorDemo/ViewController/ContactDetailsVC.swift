//
//  ContactDetailsVC.swift
//  CalculatorDemo
//
//  Created by PiPyL on 10/6/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class ContactDetailsVC: UIViewController { //hien thi 3 lan

    @IBOutlet weak var mailView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var avatarView: UIView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var jobLabel: UILabel!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    var contactModel: ContactModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        self.navigationItem.title = contactModel?.name

        nameLabel.text = contactModel?.name
        phoneLabel.text = contactModel?.phone
        mailLabel.text = contactModel?.mail
        jobLabel.text = contactModel?.job
        addressLabel.text = contactModel?.address
        
        contentView.layer.shadowColor = UIColor.gray.cgColor
        contentView.layer.shadowRadius = 7
        contentView.layer.shadowOpacity = 1
        contentView.layer.shadowOffset = CGSize(width: 0, height: 0)
        contentView.layer.cornerRadius = 12
                
        avatarView.layer.cornerRadius = avatarView.frame.width / 2
        avatarView.layer.shadowColor = UIColor.gray.cgColor
        avatarView.layer.shadowRadius = 8
        avatarView.layer.shadowOpacity = 1
        avatarView.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width / 2
        
        DispatchQueue.global().async {
            if let url = URL(string: self.contactModel?.avatar ?? ""), let data = try? Data(contentsOf: url) {
                DispatchQueue.main.async {
                    self.avatarImageView.image = UIImage(data: data)
                }
            }
        }
    }
}
