//
//  LifeCycleVC.swift
//  CalculatorDemo
//
//  Created by PiPyL on 10/6/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class LifeCycleVC: UIViewController {
    
    override func viewDidLoad() { // Chi chay dung 1 lan duy nhat sau khi duoc khoi tao
        super.viewDidLoad()
        print("View did load")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("view Will Appear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("view Did Appear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("view Will Disappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("view Did Disappear")
    }
}
