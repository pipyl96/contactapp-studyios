//
//  ContactModel.swift
//  CalculatorDemo
//
//  Created by PiPyL on 9/29/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class ContactModel: NSObject {
    var name: String {
        return (lastName ?? "") + " " + (firstName ?? "")
    } //Ho va ten
    var phone: String?
    var mail: String?
    var avatar: String?
    var address: String?
    var firstName: String?
    var lastName: String?
    var id: String?
    var job: String?
    
    init(name: String, phone: String, avatar: String, mail: String? = nil) { //khoi tao 1 doi tuong moi
        self.phone = phone
        self.avatar = avatar
        self.mail = mail
    }
    
    init(dictionary: [String: Any]) {
        if let id = dictionary["id"] as? String {
            self.id = id
        }
        if let firstName = dictionary["firstName"] as? String {
            self.firstName = firstName
        }
        if let lastName = dictionary["lastName"] as? String {
            self.lastName = lastName
        }
        if let mail = dictionary["mail"] as? String {
            self.mail = mail
        }
        if let phone = dictionary["phone"] as? String {
            self.phone = phone
        }
        if let avatar = dictionary["avatar"] as? String {
            self.avatar = avatar
        }
        if let job = dictionary["job"] as? String {
            self.job = job
        }
        if let address = dictionary["address"] as? String {
            self.address = address
        }
    }
}
