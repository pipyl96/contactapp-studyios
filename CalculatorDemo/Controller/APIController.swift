//
//  APIController.swift
//  CalculatorDemo
//
//  Created by Lê Phước on 11/10/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class APIController: NSObject {

    private let baseURL = "https://5fb334a7b6601200168f7211.mockapi.io/api/v1/contacts" //"https://api.jsonbin.io/b/5fadf0ec43fc1e2e1b417d5c/3"//"https://loicontacts.herokuapp.com/contacts"
    
    static let shared = APIController() //Singleton
    
    func getAvatar(urlString: String, completion: @escaping (UIImage?) -> ()) {
        guard let url = URL(string: urlString) else {
            completion(nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                completion(nil)
            }
            guard let data = data, let image = UIImage(data: data) else {
                completion(nil)
                return
            }
            completion(image)
        }
        task.resume()
    }
    
    func fetchContacts(completion: @escaping (Result<[ContactModel], Error>) -> ()) {
        //B1: Tao url
        guard let url = URL(string: baseURL) else {
            //TH URL loi
            completion(.failure(createError(message: "Lỗi URL!")))
            return
        }
        
        //B2: Tao URLSession
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
            }
            //B3: Chuyen du lieu tu server thanh du lieu ma chung ta muon
            guard let data = data, let list = try? JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] else {
                completion(.failure(self.createError(message: "Lỗi khi lấy danh sách danh bạ!")))
                return
            }
            //Thanh cong
            
            var contactList = [ContactModel]()
            
            list.forEach { (dict) in
                let contact = ContactModel.init(dictionary: dict)
                contactList.append(contact)
            }
            
            completion(.success(contactList))
        }
        task.resume() //Bat dau thuc hien
    }
    
    func deleteContacts(idContact: String, completion: @escaping(Error?) -> ()) {
        guard let url = URL(string: baseURL + "/" + idContact) else {
            completion(createError(message: "URL lỗi!"))
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "DELETE"
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if error != nil {
                completion(self.createError(message: "Lỗi khi xóa danh bạ!"))
            }
            
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completion(self.createError(message: "Lỗi khi xóa danh bạ!"))
                return
            }
            
            completion(nil)
        }
        
        task.resume()
    }
    
    //MARK: - Helper
    
    private func createError(message: String? = nil) -> NSError {
        return NSError(domain: "com.contacts", code: 400, userInfo: [NSLocalizedDescriptionKey: message ?? "Đã có lỗi xảy ra!"])
    }
}
