//
//  ContactTableViewCell.swift
//  CalculatorDemo
//
//  Created by PiPyL on 9/26/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    
    var indexPath: IndexPath!
    
    static let cellIdentifier: String = "ContactTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()

        avatarImageView.layer.cornerRadius = 20
        avatarImageView.clipsToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        avatarImageView.image = nil
    }
    
    func setupCell(contactModel: ContactModel, indexPath: IndexPath) {
        self.indexPath = indexPath
        nameLabel.text = contactModel.name
        phoneLabel.text = contactModel.phone
        
        if let avatar = contactModel.avatar, avatar != "" {
            getAvatar(urlString: avatar, indexPath: indexPath)
        }
    }
    
    private func getAvatar(urlString: String, indexPath: IndexPath) {
        APIController.shared.getAvatar(urlString: urlString) { [weak self] (avatar) in
            guard let self = self else { return }
            if let avatar = avatar, self.indexPath == indexPath {
                DispatchQueue.main.async {
                    self.avatarImageView.image = avatar
                }
            }
        }
    }
    
    @IBAction func didTapCallButton(_ sender: Any) {
        
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "ContactTableViewCell", bundle: nil)
    }
}
